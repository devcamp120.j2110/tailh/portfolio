import React from "react";
// Style
import "./portfolio.css";
// Image
import IMG1 from "../../assets/portfolio1.jpg";
import IMG2 from "../../assets/portfolio2.jpg";
import IMG3 from "../../assets/portfolio3.jpg";
const Portfolio = () => {
  return (
    <section id="portfolio">
      <h5>My Project</h5>
      <h2>Portfolio</h2>
      <div className="container portfolio__container">
        {/* Shop 24h */}
        <article className="portfolio__item">
          <div className="portfolio__item-image">
            <img src={IMG1} alt="portfolio" />
          </div>
          <h3>Kara Car Shop</h3>
          <div className="portfolio__item-cta">
            <a
              href="https://gitlab.com/devcamp120.j2110/tailh"
              className="btn btn-primary"
              target="_blank"
            >
              GitLab
            </a>
          </div>
        </article>
        {/* Pizza */}
        <article className="portfolio__item">
          <div className="portfolio__item-image">
            <img src={IMG2} alt="portfolio" />
          </div>
          <h3>Pizza 365 Shop</h3>
          <div className="portfolio__item-cta">
            <a
              href="https://gitlab.com/devcamp120.j2110/tailh"
              className="btn btn-primary"
              target="_blank"
            >
              GitLab
            </a>
          </div>
        </article>
        {/* Weather App */}
        <article className="portfolio__item">
          <div className="portfolio__item-image">
            <img src={IMG3} alt="portfolio" />
          </div>
          <h3>Weather App</h3>
          <div className="portfolio__item-cta">
            <a
              href="https://gitlab.com/devcamp120.j2110/tailh"
              className="btn btn-primary"
              target="_blank"
            >
              GitLab
            </a>
          </div>
        </article>
      </div>
    </section>
  );
};

export default Portfolio;
