import React from "react";
// Style
import "./header.css";
// Component
import CTA from "./CTA";
import HeaderSocials from "./HeaderSocials";
// IMG
import ME from "../../assets/me.png";

const Header = () => {
  return (
    <header>
      <div className="container header__container">
        <h5>Hello I'm</h5>
        <h1 style={{fontWeight:"bold" , letterSpacing:"2px"}}>Hữu Tài</h1>
        <h5 className="text-light">Frontend Developer</h5>
        <CTA />
        <HeaderSocials/>
        <div className="me">
          <img src={ME} alt="me" />
        </div>
        <a href="#contact" className="scroll__down">
          Scroll Down
        </a>
      </div>
    </header>
  );
};

export default Header;
