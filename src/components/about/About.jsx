import React from "react";
// Style
import "./about.css";
// IMG
import ME from "../../assets/me-about.jpg";
// Icon
import { FaAward } from "react-icons/fa";
import { AiTwotoneExperiment } from "react-icons/ai";
import { VscFolderLibrary } from "react-icons/vsc";

const About = () => {
  return (
    <section id="about">
      <h5>Get To Know</h5>
      <h2>About Me</h2>
      <div className="container about__container">
        <div className="about__me">
          <div className="about__me-image">
            <img src={ME} alt="About Img" />
          </div>
        </div>
        <div className="about__content">
          <div className="about__cards">
            {/* Experience */}
            <article className="about__card">
              <AiTwotoneExperiment className="about__icon" />
              <h5>Experience</h5>
              <h5 className="small">6 Months</h5>
            </article>
            {/* Experience */}
            <article className="about__card">
              <FaAward className="about__icon" />
              <h5>Graduate</h5>
              <h5 className="small">IronHack Devcamp</h5>
            </article>
            {/* Experience */}
            <article className="about__card">
              <VscFolderLibrary className="about__icon" />
              <h5>Project</h5>
              <h5 className="small">3+ Completed</h5>
            </article>
          </div>
          <p>
            "Là một người yêu thích lập trình và đến nay đã hơn 6 tháng, xuất
            phát điểm là một người chuyển ngành , không biết gì về lập trình .
            Đến nay cũng đã tự mình code được nhiều dự án cá nhân từ nhỏ đến
            lớn. Dù chưa có kinh nghiệm thực tế nào , nhưng trong khoảng thời
            gian học ở trung tâm và tự học đã gặt hái được không ít kiến thức.
            Mong muốn được làm việc trong môi trường IT . Cố gắng hết sức để
            xứng đáng là lựa chọn của nhà tuyển dụng."
          </p>
          <a href="#contact" className="btn btn-primary">
            Let's Talk
          </a>
        </div>
      </div>
    </section>
  );
};

export default About;
